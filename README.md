# About this repository

This repository contained LUCI project config files for a "tricium" LUCI
project, which since been removed.

The reason for originally having a separate project was for security and
organizational purposes, so that Tricium builders can be kept separate from
other LUCI projects like "infra", since Tricium analysis jobs don't need access
to things like logs in other projects.

This became unnecessary with transition to recipe-based analyzers where all
Tricium builders builders live in their respective projects, e.g. "fuchsia"
and "chromium".

See also https://crbug.com/1008461.
